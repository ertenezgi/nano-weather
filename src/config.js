export const baseImg = 'http://openweathermap.org/img/wn/';

export const initCity = [
  { city: 'Madrid', country: 'Spain', key: 'es' },
  { city: 'London', country: 'England', key: 'gb' },
  { city: 'Paris', country: 'France', key: 'fr' },
  { city: 'Berlin', country: 'Germany', key: 'de' },
  { city: 'Roma', country: 'Italy', key: 'it' },
];

