import Vue from 'vue'
import App from './App.vue'
import router from "./router";
import store from './store'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faWind, faChevronDown, faHome } from '@fortawesome/free-solid-svg-icons'


Vue.config.productionTip = false
library.add(faWind, faChevronDown, faHome)

Vue.component('font-awesome-icon', FontAwesomeIcon)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
