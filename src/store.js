import Vue from 'vue';
import Vuex from 'vuex';
import AppServices from '@/services/AppServices';

Vue.use(Vuex);

const state = {
  currentWeather: null,
  forecastWeather: [],
  city: '',
  country: '',
};

const getters = {
  temp: (state) =>
    state.currentWeather != null ? state.currentWeather.main.temp : null,
  weather: (state) =>
    state.currentWeather != null ? state.currentWeather.weather[0] : null,
    wind: (state) =>
    state.currentWeather != null ? state.currentWeather.wind.speed : null,
};

const mutations = {
  UPDATE_CITY(state, value) {
    state.city = value.city;
    state.country = value.country;
  },
  UPDATE_WEATHER(state, { current, forecast }) {
    Vue.set(state, 'currentWeather', current);
    Vue.set(state, 'forecastWeather', forecast);
  },
};

const actions = {
  async FETCH_WEATHER({ commit }, value) {
    commit('UPDATE_CITY', value);

    let current = null;
    let forecast = [];

    if (value.key != '') {
      try {
        const currentWeather = await AppServices.getCurrentWeather({
          city: value.city,
        });
        current = currentWeather.data != '' ? currentWeather.data : null;
      } catch (e) {
        current = null;
      }

      try {
        const forecastWeather = await AppServices.getForecastWeather({
          city: value.city,
        });

        forecast = forecastWeather.data != '' ? forecastWeather.data.list : [];
        let forecastfive = [];
        forecastfive = forecast
          .slice(0, 1)
          .concat(
            forecast.slice(8, 9),
            forecast.slice(16, 17),
            forecast.slice(24, 25),
            forecast.slice(32, 33)
          );
        forecast = forecastfive;
      } catch (e) {
        forecast = [];
      }
    }
    commit('UPDATE_WEATHER', { current, forecast });
  },
};

const store = new Vuex.Store({
  state,
  mutations,
  actions,
  getters,
});

export default store;
