import { API, key } from '@/services/api';

export default {
  getCurrentWeather(params) {
    return API.get(`weather?q=${params.city}&appid=${key}&units=metric`);
  },
  getForecastWeather(params) {
    return API.get(`forecast?q=${params.city}&appid=${key}&units=metric`);
  },
};
