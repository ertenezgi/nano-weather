# weather-app

A simple weather app made with Vue.js.

## Built With
- [`vue`](https://github.com/vuejs/vue), [`vuex`](https://github.com/vuejs/vuex), [`vue-router`](https://github.com/vuejs/vue-router)
- [`axios`](https://github.com/axios/axios)
- [`day.js`](https://github.com/iamkun/dayjs)
- [`core-js`](https://github.com/zloirock/core-js)
- [`font-awesome`](https://github.com/FortAwesome/Font-Awesome)
- (DEV) [`node-sass`](https://github.com/sass/node-sass), [`sass-loader`](https://github.com/webpack-contrib/sass-loader)

## Description

Main goal of the application isto display a weather information for five pre-coded cities. Aforementioned information includes main weather, weather description and wind speed. There is also a toggle-able button which opens a five-day forecast for the selected city.

Application uses:

- BAM Methodology
- Grid Layout

## Known Issues

- Even though application makes a call to the API and gets the data for 5 different cities and their forecasts, due to not some Vuex state management issues it displays the latest arriving one's data to the component.

## Notes

- Built using [`vue-cli`](https://github.com/vuejs/vue-cli)

- Application uses [OpenWeatherAPI](https://openweathermap.org/api). One must get a key from their website by signing up and populate the `key` const in `src/services/api`.
## Start-up

### Installing dependencies:

```
npm install
```

### Serving the app in development:

```
npm run serve
```

### Building the app for production:

```
npm run serve
```
